import numpy
import setuptools
from Cython.Build import cythonize
from Cython.Compiler import Options


DEBUG = False
Options.docstrings = True
Options.embed_pos_in_docstring = False
Options.generate_cleanup_code = False
Options.clear_to_none = True
Options.annotate = True
Options.fast_fail = False
Options.warning_errors = False
Options.error_on_unknown_names = True
Options.error_on_uninitialized = True
Options.convert_range = True
Options.cache_builtins = True
Options.gcc_branch_hints = True
Options.lookup_module_cpdef = False
Options.embed = None
Options.cimport_from_pyx = False
Options.buffer_max_dims = 8
Options.closure_freelist_size = 8


setuptools.setup(
    name='farms_container',
    version='0.1',
    description='Data container class for farms experiments',
    url='https://gitlab.com/FARMSIM/farms_container.git',
    author='biorob-farms',
    author_email='biorob-farms@groupes.epfl.ch',
    license='Apache-2.0',
    packages=setuptools.find_packages(exclude=['tests*']),
    install_requires=[
        'numpy',
        'Cython',
        'farms_pylog @ git+https://gitlab.com/FARMSIM/farms_pylog.git',
        'pandas',
    ],
    ext_modules=cythonize(
        "farms_container/*.pyx",
        compiler_directives={
            # Directives
            'binding': False,
            'embedsignature': True,
            'cdivision': True,
            'language_level': 3,
            'infer_types': True,
            'profile': DEBUG,
            'wraparound': False,
            'boundscheck': DEBUG,
            'nonecheck': DEBUG,
            'initializedcheck': DEBUG,
            'overflowcheck': DEBUG,
            'overflowcheck.fold': DEBUG,
            'cdivision_warnings': DEBUG,
            'always_allow_keywords': DEBUG,
            'linetrace': DEBUG,
            # Optimisations
            'optimize.use_switch': True,
            'optimize.unpack_method_calls': True,
            # Warnings
            'warn.undeclared': True,
            'warn.unreachable': True,
            'warn.maybe_uninitialized': True,
            'warn.unused': True,
            'warn.unused_arg': True,
            'warn.unused_result': True,
            'warn.multiple_declarators': True,
        }
    ),
    zip_safe=False,
    package_data = {
        'farms_container': ['*.pxd'],
    },
    include_dirs=[numpy.get_include()]
)
